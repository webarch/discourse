# vBulletin 4 Import

To configure a Discourse server to import from vBulletin 4 following [the notes
in this Discourse forum
post](https://meta.discourse.org/t/importing-from-vbulletin-4/54881)

First follow the install instructions above and then manually change these settings in the Discourse admin interface:

* Change the value of `slug_generation_method` from `ascii` to `encoded`.
* Enable `login_required` (recommended. At least to finish the importing)
* Disable `download_remote_images_to_local` if you don't want Discourse to download images embedded in posts.
* Enable `disable_edit_notifications` if you enabled `download_remote_images_to_local` and don't want your users to get lots of notifications about posts edited by the system user.

Dump the database on the old server:

```bash
mysqldump -uroot -p database -r dump.sql
```

Check the encoding:

```bash
uchardet dump.sql
UTF-8
iconv -f utf-8 -t utf-8 dump.sql > dump.utf-8.sql
iconv: illegal input sequence at position 29618109
```

Make a directory for the database dump on the host running Docker:

```bash
mkdir /var/discourse/shared/standalone/import/
```

This can be accessed from the Docker container at `/shared`, copy the database dump to this directory and the `album` directory of thumbnails.

Then:

```bash
su - discourse
cd /var/discourse
./launcher enter app
cd /var/www/discourse
apt-get update
apt-get install libmysqlclient-dev mysql-server-5.7
echo "gem 'mysql2', require: false" >> /var/www/discourse/Gemfile
echo "gem 'php_serialize', require: false" >> /var/www/discourse/Gemfile
echo "discourse ALL = NOPASSWD: ALL" >> /etc/sudoers
su discourse -c 'bundle install --no-deployment --without test --without development'
mysql -uroot -p -e 'CREATE DATABASE vb4 CHARACTER SET utf8'
cd /shared
mysql -uroot -p --default-character-set=utf8 vb4
  mysql> SET names 'utf8';
  mysql> SOURCE dump.sql;

```

Edit the `script/import_scripts/vbulletin.rb` script and change these variables to suit:

```bash
  DB_HOST ||= ENV['DB_HOST'] || "localhost"
  DB_NAME ||= ENV['DB_NAME'] || "vb4"
  DB_PW ||= ENV['DB_PW'] || ""
  DB_USER ||= ENV['DB_USER'] || "root"
  TIMEZONE ||= ENV['TIMEZONE'] || "Europe/London"
  TABLE_PREFIX ||= ENV['TABLE_PREFIX'] || ""
  ATTACHMENT_DIR ||= ENV['ATTACHMENT_DIR'] || '/shared/import/album'
```

Clear the existing database:

```bash
cd /var/www/discourse
su discourse -c 'DISABLE_DATABASE_ENVIRONMENT_CHECK=1 bundle exec rake db:drop db:create db:migrate'
```

Run the import script:

```bash
su discourse -c 'RAILS_ENV=production ruby script/import_scripts/vbulletin.rb'
```
