# Webarchitects Discourse Ansible role

An Ansible role to install and configure [Discourse](https://www.discourse.org/) and Postfix for incoming and outgoing email on Debian.

This repo uses the [Webarchitects Docker Ansible role](https://git.coop/webarch/docker) to install Docker CE.

The email setup is based on the [mail-reciever Docker container](https://github.com/discourse/mail-receiver) plus [this pull request](https://github.com/discourse/mail-receiver/pull/2) (which is now merged) and the [Postfix notes for using the host for outgoing email](https://meta.discourse.org/t/emails-with-local-smtp/23645/28), with an additional [Ruby script](https://git.coop/webarch/discourse/blob/master/roles/email/files/discourse-smtp-rcpt-acl).

This role has been written with the assumption that a few other [Webarchitects roles](https://git.coop/webarch) will also be used, for example:

```yml
- name: Install Discourse
  become: false
  remote_user: root
  gather_facts: true

  hosts:
    - discourse_servers

  vars:
    docker_compose: false
    docker_compose_v1: false
    docker_nameservers:
      - 1.1.1.1
      - 9.9.9.9
    discourse_admin_email: admin@example.org

  roles:
    - apt
    - jc
    - systemd
    - docker
    - sudo
    - vim
    - discourse
```

## Configuration

Create an admin account, see [meta.discourse.org for details](https://meta.discourse.org/t/create-admin-account-from-console/17274):

```bash
su - discourse
cd /var/discourse
rake admin:create
exit
```

To configure incoming email, int he admin interface set:

* `reply by email enabled` tick *"Enable replying to topics via email."*
* `reply by email address` set this to `discourse+%{reply_key}@$SERVERNAME` (use the actual domain name not $SERVERNAME)
* `manual polling enabled` tick *"Push emails using the API for email replies."*

Then generate an API key at `https://$SERVERNAME/admin/api/keys` with all permissions (it doesn't appear to work with only *"receive emails"* permission) and set the `discourse_api_key` variable and then re-run this role.

Other settings:

* `disable mailing list mode` unselect this option if you want users to be able to opt into mailing list mode.

## Discourse Upgrade

To upgrade Discourse use the `discourse_upgrade` Ansible tag and set `discourse_upgrade` to True if you want the container to be rebuilt when there are no changes to the Discourse code.

### PostgreSQL 15 update

See the [PostgreSQL 15 update](https://meta.discourse.org/t/postgresql-15-update/349515) thread which conteins these instructions:

```bash
# Backup the server!
sudo -i
chown -R 101:104 /var/discourse/shared/standalone/postgres_data
su - discourse
cd /var/discourse
git stash
git stash drop
git pull
./launcher stop app
docker run --rm \
  --entrypoint=/bin/bash \
  -e LANG='en_GB.UTF-8' \
  -v /var/discourse/shared/standalone/postgres_data:/var/lib/postgresql/13/data \
  -v /var/discourse/shared/standalone/postgres_data_new:/var/lib/postgresql/15/data \
  tianon/postgres-upgrade:13-to-15 \
  -c 'sed -i "s/^# $LANG/$LANG/" /etc/locale.gen && locale-gen &&
  apt-get update && apt-get install -y postgresql-13-pgvector postgresql-15-pgvector &&
  docker-upgrade'
exit
mv /var/discourse/shared/standalone/postgres_data /var/discourse/shared/standalone/postgres_data_old
mv /var/discourse/shared/standalone/postgres_data_new /var/discourse/shared/standalone/postgres_data
chown -R 101:104 /var/discourse/shared/standalone/postgres_data
su - discourse
cd /var/discourse
docker run --rm -v /var/discourse/shared/standalone:/shared \
local_discourse/app chown -R postgres:postgres /shared/postgres_data
./launcher rebuild app
```

## Troubleshooting

* [Create a admin account using the console](https://meta.discourse.org/t/create-admin-account-from-console/17274)
* You can bypass things like SSO, as an admin using a `https://SERVERDOMAIN/u/admin-login/` URL to generate a email that contains a login URL

## TODO

* Move the Postfix and OpenDKIM tasks into a generic Postfox role.

## Repo history

The initial import of files in this repo was copied from the [CoTech Ansible](https://git.coop/cotech/ansible/-/tree/ce2def1d125972f693db7f87173e32c5d262104d) repo.

In 2022 the code in this repo was refactored to simply provide a role, [prior to that](https://git.coop/webarch/discourse/-/tree/d783c0ba2fbcf25ab22b627622da512c32c4dc9d) it contained multiple roles.
